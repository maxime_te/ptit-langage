	

    (* D�finition de l'arbre syntaxique *)
    type instrs =
            | Instrs of instr * instrs
            | Nothing
    and instr =
            | Let of string * expr
            | Print of expr
    and expr =
            | Id of string
            | Nbi of int
            | Nbf of float
            | Calc of string * expr * expr
           
    (* Analyse lexicale *)
    open Genlex
    let analex = function
            | "" -> failwith "[analex] -> Source vide"
            | src -> make_lexer ["let"; "print"; "("; ")"; "+"; "-"; "/"; "*"] (Stream.of_string src)
           
    (* Analyse syntaxique *)
    let anasynt flt =
            let rec parse_instrs = parser
                    | [< 'Kwd "("; i = parse_instr; 'Kwd ")"; is = parse_instrs >] -> Instrs (i, is)
                    | [< >] -> Nothing
                    | [< >] -> failwith "[anasynt][parse_instrs] -> Erreur"
            and parse_instr = parser
                    | [< 'Kwd "let"; 'Ident id; e = parse_expr >] -> Let (id, e)
                    | [< 'Kwd "print"; e = parse_expr >] -> Print e
                    | [< >] -> failwith "[anasynt][parse_instr] -> Erreur"
            and parse_expr = parser
                    | [< 'Ident id >] -> Id id
                    | [< 'Int i >] -> Nbi i
                    | [< 'Float f >] -> Nbf f
                    | [< 'Kwd "("; 'Kwd op when isOp op; exp1 = parse_expr; exp2 = parse_expr; 'Kwd ")" >] -> Calc (op, exp1, exp2)
                    | [< >] -> failwith "[anasynt][parse_expr] -> Erreur"
            and isOp = function
                    | ("+" | "-" | "/" | "*") -> true
                    | _ -> false
            in parse_instrs flt
           
    (* Evaluation *)
    let eval ast table =
            let rec eval_instrs = function
                    | Instrs (i, is) -> eval_instr i; eval_instrs is
                    | Nothing -> ()
            and eval_instr = function
                    | Let (id, e) -> if Hashtbl.mem table id then failwith ("[eval] -> Variable : " ^ id ^ " deja definie") else Hashtbl.add table id (eval_expr e)
                    | Print e -> print_float (eval_expr e); print_newline()
            and eval_expr = function
                    | Id id -> if Hashtbl.mem table id then Hashtbl.find table id else failwith ("[eval] -> Variable : " ^ id ^ " non definie")
                    | Nbi i -> float_of_int i
                    | Nbf f -> f
                    | Calc (op, exp1, exp2) ->
                            let a, b = eval_expr exp1, eval_expr exp2
                            in (match op with
                                            | "-" -> a -. b
                                            | "+" -> a +. b
                                            | "/" -> a /. b
                                            | "*" -> a *. b
                                            | _ -> failwith "Operateur inconnu")
            in eval_instrs ast
           
    (* Main *)
    let () =
            let read_bloc () =
                    let rec read acc = match read_line () with
                            | ";;" -> acc
                            | s -> read (s :: acc)
                    in String.concat " " (List.rev (read []))
            in let src = read_bloc ()
            in eval (anasynt (analex src)) (Hashtbl.create 1)


